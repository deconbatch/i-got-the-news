/**
 * I Got The News.
 * draw three wave patterns with my custom noise fomula.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.06.01
 */

void setup() {
  size(1080, 670);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();
}

void draw() {

  float baseHue      = random(360);
  float marginHeight = height / 5.0;
  float axisHeight   = (height - marginHeight * 2.0) / 2.0;

  // make three image files
  for (int imgCnt = 1; imgCnt <= 3; ++imgCnt) {

    baseHue += 45.0;
    background(0.0, 0.0, 100.0, 100.0);

    pushMatrix();
    translate(width * 0.5, marginHeight - axisHeight);
    for (int lineCnt = 0; lineCnt < 3; lineCnt++) {
      translate(0.0, axisHeight);
      baseHue += 120.0;

      drawWave(baseHue, 90.0, 80.0, 3.0);
      drawWave(baseHue + 15.0, 60.0, 70.0, 50.0);
      drawWave(baseHue + 30.0, 40.0, 90.0, 80.0);
    }
    popMatrix();
  
    casing();
    saveFrame("frames/" + String.format("%04d", imgCnt) + ".png");
  }

  exit ();

}

/**
 * drawShape : 
 * @param  _waveHue    0.0 - 360.0 : wave color.
 * @param  _waveSat    0.0 - 100.0 : wave saturation.
 * @param  _waveBri    0.0 - 100.0 : wave brightness.
 * @param  _density  any : wave frequency base value, the bigger lower frequensy.
 */
private void drawWave(float _waveHue, float _waveSat, float _waveBri, float _density) {

  float waveAlp     = 50.0;
  float waveHeight  = height / 7;
  float shapeFactor = TWO_PI * ceil(5 + random(30));  // magic number for nice shape

  noStroke();
  fill(_waveHue % 360.0, _waveSat, _waveBri, waveAlp);

  beginShape();
  for (float waveX = -width * 0.5; waveX <= width * 0.5; waveX += _density) {
    float waveY = customNoise(shapeFactor) * waveHeight;
    curveVertex(waveX, waveY);
    shapeFactor += 0.003 * _density;	// IMPORTANT
  }
  endShape();

}

/**
 * customNoise : returns -1.0 .. 1.0 almost random but interesting value
 */
private float customNoise(float _v) {
  return pow(sin(_v), 3) * cos(pow(_v, 2));
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(42.0);
  stroke(0.0, 0.0, 20.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(40.0);
  stroke(0.0, 0.0, 90.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
